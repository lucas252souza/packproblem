# -*- coding: utf-8 -*-
"""
Spyder Editor

Este é um arquivo de script temporário.
"""

import numpy as np

def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x)

    return [lista]


def pack_generator(n):
    """
    Entrada:
        n: Numero de variaveis.
    Saida:
        Z: Funcao objetivo.
        A: Matriz de Aleatorios.
        B: Matriz de ums.
        C, D: Vetores
    """
    #---- Prepara os dados
    vet_a = []
    mat_zeros = []
    mat_zeros_b = []
    vet_v = []
    j = 0
    for i in range(n):
        vet_v.append(np.dot(-1, np.random.randint(500, 10000, 1)))
        mat_zeros.append(np.zeros((n, n), dtype=int))
        mat_zeros_b.append(np.zeros((n, n), dtype=int))
        
    while j < n:
        a = np.random.randint(0, 500, n)
        if sum(a) > (-1*vet_v[j]):
            continue
        
        vet_a.append(a)
        j += 1
    
    #---- Prepara as Matrizes
    mat_v = np.zeros((n, n), dtype=int)
    for i in range(n):
        for j in range(n):
            if i == j:
                mat_v[i, j] = vet_v[i]
    
    vet_um = np.ones(n)                 
    for i in range(n):
        mat_zeros[i][i] = vet_a[i]
        mat_zeros_b[i][i] = vet_um[i]
    
    A = mat_zeros[0]
    B = mat_zeros_b[0]
    zero = np.zeros((n, n), dtype=int)
    for i in range(1, n):
        A = np.concatenate((A, mat_zeros[i]), axis=1)
        B = np.concatenate((B, mat_zeros_b[i]), axis=1)
        
    A = np.concatenate((A, mat_v), axis=1)
    B = np.concatenate((B, zero), axis=1)
    
    #---- Prepara vetores
    x = np.shape(A)[1] - n
    vet_zero = np.zeros(x, dtype=int)
    Z = np.concatenate((vet_zero, vet_um))
    
    # Concatena o vetor Z para ser a primeira linha da matriz A
    A = np.concatenate((to_list(Z), A))

    C = np.ones(np.shape(A)[0], dtype=int)
    C = np.dot(-1, C)
    C[0] = -1 * C[0]
    
    D = np.ones(np.shape(B)[0], dtype=int)
    
    return (Z, A, B, C, D)
