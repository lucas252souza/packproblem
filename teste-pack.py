# -*- coding: utf-8 -*-
"""
@author: Lucas
"""

from benders import benders_decomposition_linear_original
from benders import benders_decomposition_linear_original_bounded
from pack_generator import pack_generator
from scipy.optimize import linprog
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def prepara_dual(z, A, B, c, d):
    funcao_objetivo = np.concatenate((d, c))
    restricao = np.concatenate((B.T, A.T), axis=1)
    funcao_objetivo = np.dot(-1, funcao_objetivo)
    bound =   [(None, None),]*d.size + [(0, None), ] * c.size
    return (funcao_objetivo, restricao, z, bound)

# Variaveis usada para os testes
z, A, B, c, d = pack_generator(3)
objetivo, restricao, vetor, limite = prepara_dual(z, A, B, c, d)

# Simplex Primal
primal = linprog(c=z, A_ub=-1*A, b_ub=-1*c, A_eq=B, b_eq=d)
primal

# Simplex Dual
dual = linprog(c=objetivo, A_ub=restricao, b_ub=vetor, bounds=limite)
dual

# Benders
(x0, x, y, it, n) = benders_decomposition_linear_original(d, c, B.T, A.T, z)
(x0, x, y, it, n) = benders_decomposition_linear_original_bounded(d, c, B.T, A.T, z, 1000000)

#---------------------------------------------------------------------------------------------------------

def teste(maximo):
    primais = []
    duais = []
    primal_it = []
    dual_it = []
    benders_it = []
    benders = []
    i = 5
    j = 0

    while True:
        print(i)
        #Gera aleatorio
        z, A, B, c, d = pack_generator(i)
        objetivo, restricao, vetor, limite = prepara_dual(z, A, B, c, d)
        
        #Executa simplexes
        primal = linprog(c=z, A_ub=np.dot(-1, A), b_ub=np.dot(-1, c), A_eq=B, b_eq=d)
        dual = linprog(c=objetivo, A_ub=restricao, b_ub=vetor, bounds=limite)
        
        #Executa Benders
        (x0, x, y, it, n) = benders_decomposition_linear_original(d, c, B.T, A.T, z)
        #if np.isinf(x0):
        #    continue
        
        #Salva dados
        primais.append(primal.fun)
        duais.append(dual.fun)
        benders.append(x0)
        primal_it.append(primal.nit)
        dual_it.append(dual.nit)
        benders_it.append(n)
        
        j+=1
        if j % 5 == 0:
            i += 5
        if i == maximo:
            break
        
    df = pd.DataFrame()
        
    df["Primais"] = primais
    df["Duais"] = duais
    df["Benders"] = benders
    df["Primal_it"] = primal_it
    df["Dual_it"] = dual_it
    df["Benders_it"] = benders_it
        
    return df
    
t = teste(20)
t.to_excel("Pack.xlsx")

plt.plot(t.index, t["Primal_it"])
plt.plot(t.index, t["Dual_it"])
plt.show()